package pl.edu.anstar.milionerzy.ui;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import pl.edu.anstar.milionerzy.database.CSVController;
import pl.edu.anstar.milionerzy.database.Question;
import pl.edu.anstar.milionerzy.database.Question.Answer;

public class QuestionsController implements Initializable {
	private static Question currentQuestion = null;
	private Boolean isCorrectAnswerSelected = null;
	@FXML
	Button answer1Button;
	@FXML
	Button answer2Button;
	@FXML
	Button answer3Button;
	@FXML
	Button answer4Button;
	@FXML
	Button confirmButton;
	@FXML
	Text currentPrizeText;
	@FXML
	Button fiftyfiftyButton;
	@FXML
	Button askAudienceButton;
	@FXML
	Button callFriendButton;
	@FXML
	Text questionStringText;
	@FXML
	Text questionNumber;
	@FXML
	Text helpText;

	/**
	 * Method used to initialize QuestionsController fxml and methods from there
	 * @param location
	 * The location used to resolve relative paths for the root object, or
	 * {@code null} if the location is not known.
	 *
	 * @param resources
	 * The resources used to localize the root object, or {@code null} if
	 * the root object was not localized.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setUpQuestion();
	}

	/**
	 * Method used to set answers for fxml class and operate on csv controller
	 */
	private void setUpQuestion() {
		if (currentQuestion == null) {
			currentQuestion = CSVController.getRandomQuestion(1);
		} else {
			currentQuestion = CSVController
					.getRandomQuestion(currentQuestion.getQuestionNumber() + 1);
		}
		this.questionStringText.setText(Objects.requireNonNull(currentQuestion).getQuestionString());
		this.questionNumber.setText(currentQuestion.getQuestionNumber() + " ");
		this.answer1Button.setText(currentQuestion.getAnswer(Answer.FIRST));
		this.answer2Button.setText(currentQuestion.getAnswer(Answer.SECOND));
		this.answer3Button.setText(currentQuestion.getAnswer(Answer.THIRD));
		this.answer4Button.setText(currentQuestion.getAnswer(Answer.FOURTH));
		this.currentPrizeText.setText(currentQuestion.getPrize() + "$");
		resetButtonsColor();
		this.answer1Button.setVisible(true);
		this.answer2Button.setVisible(true);
		this.answer3Button.setVisible(true);
		this.answer4Button.setVisible(true);
		isCorrectAnswerSelected = null;
	}

	/**
	 * Method used to reset to default color of answers buttons
	 */
	private void resetButtonsColor() {
		answer1Button.setStyle("-fx-background-color: gold");
		answer2Button.setStyle("-fx-background-color: gold");
		answer3Button.setStyle("-fx-background-color: gold");
		answer4Button.setStyle("-fx-background-color: gold");
	}

	/**
	 *  Method which informs in front which button is chosen by player.
	 * @param event
	 */
	@FXML
	private void answerButtonSelected(ActionEvent event) {
		Button selectedButton = (Button) event.getSource();
		this.isCorrectAnswerSelected = switch (currentQuestion.getCorrect()) {
			case 1 -> selectedButton == answer1Button;
			case 2 -> selectedButton == answer2Button;
			case 3 -> selectedButton == answer3Button;
			case 4 -> selectedButton == answer4Button;
			default -> false;
		};
		resetButtonsColor();
		selectedButton.setStyle("-fx-background-color: pink");
	}

	/**
	 *	Method of confirming button, it checks option and give next instructions
	 * @param event
	 */
	@FXML
	private void confirmButtonSelected(ActionEvent event) {
		try {
			if (isCorrectAnswerSelected == null) {
				return;
			}
			Parent root;
			if (isCorrectAnswerSelected) {
				if (currentQuestion.getPrize() != 1000000) {
					setUpQuestion();
					this.helpText.setText(" ");
					return;
				}
				root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("summary.fxml")));
			} else {
				root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("loose.fxml")));
			}
			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			Scene scene = new Scene(root);
			stage.setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method is showing correct answer and one on the cross of correct
	 */
	@FXML
	private void fireFiftyFiftyButton() {
		if (!this.answer1Button.getText().equals(this.currentQuestion.getAnswersArray()[0]) ||
				!this.helpText.getText().equals(" ") || this.helpText.getText().equals("Nie możesz użyć tych kół ratunkowych razem")) {
			this.helpText.setText("Nie możesz użyć tych kół ratunkowych razem");
		} else {
			this.fiftyfiftyButton.setDisable(true);
			switch (currentQuestion.getCorrect()) {
				case 1, 4 -> {
					this.answer2Button.setVisible(false);
					this.answer3Button.setVisible(false);
				}
				case 2, 3 -> {
					this.answer1Button.setVisible(false);
					this.answer4Button.setVisible(false);
				}
			}
		}
	}
	/**
	 *  Method used to generate list of int used in showing which option is correct in game
	 */
	@FXML
	private void fireAskAudienceButton() {
		if(!this.answer1Button.isVisible() || !this.answer2Button.isVisible()){
			this.helpText.setText("Nie możesz użyć tych kół ratunkowych razem");
		}else{
			askAudienceButton.setDisable(true);
			int min = 50;
			List<Integer> results = new ArrayList<>();
			Random rand = new Random();
			int max = 100;
			byte iter = 0;
			boolean test = false;
			boolean test2 = false;
			int temp;
			do {
				if (!test && currentQuestion.getAnswersArray()[iter].equals(currentQuestion
						.getAnswersArray()[currentQuestion.getCorrect() - 1])&& !test2){
					temp = rand.ints(min, max).findFirst().getAsInt();
					max = max - temp;
					min = 1;
					test = true;
					results.add(temp);
				} else if (test) {
					temp = rand.ints(min, max).findFirst().getAsInt();
					max = max - temp;
					results.add(temp);
					if(results.size()==3){
						test = false;
						test2 = true;
					}

				} else if (results.size()==3){
					results.add(max);
				}
				else {
					iter++;
				}
			} while (results.size() != 4);
			switch (currentQuestion.getCorrect()) {
				case 1 -> {
					this.answer1Button.setText(this.answer1Button.getText() + " " + results.get(0) + " %");
					this.answer2Button.setText(this.answer2Button.getText() + " " + results.get(1) + " %");
					this.answer3Button.setText(this.answer3Button.getText() + " " + results.get(2) + " %");
					this.answer4Button.setText(this.answer4Button.getText() + " " + results.get(3) + " %");
				}
				case 2 -> {
					this.answer2Button.setText(this.answer2Button.getText() + " " + results.get(0) + " %");
					this.answer1Button.setText(this.answer1Button.getText() + " " + results.get(1) + " %");
					this.answer3Button.setText(this.answer3Button.getText() + " " + results.get(2) + " %");
					this.answer4Button.setText(this.answer4Button.getText() + " " + results.get(3) + " %");
				}
				case 3 -> {
					this.answer3Button.setText(this.answer3Button.getText() + " " + results.get(0) + " %");
					this.answer2Button.setText(this.answer2Button.getText() + " " + results.get(1) + " %");
					this.answer1Button.setText(this.answer1Button.getText() + " " + results.get(2) + " %");
					this.answer4Button.setText(this.answer4Button.getText() + " " + results.get(3) + " %");

				}
				case 4 -> {
					this.answer4Button.setText(this.answer4Button.getText() + " " + results.get(0) + " %");
					this.answer2Button.setText(this.answer2Button.getText() + " " + results.get(1) + " %");
					this.answer1Button.setText(this.answer1Button.getText() + " " + results.get(2) + " %");
					this.answer3Button.setText(this.answer3Button.getText() + " " + results.get(3) + " %");
					}
				}
		}
	}
	/**
	 * Method use to get random number of Call a Friend lifeline
	 */
	@FXML
	private void fireCallFriendButton() {
		Random rand = new Random();
		if (currentQuestion.getPrize() < 20000) {
			this.callFriendButton.setDisable(true);
			this.helpText
					.setText(currentQuestion.getAnswersArray()[currentQuestion.getCorrect() - 1]);
		} else {
			if(!this.answer1Button.isVisible() || !this.answer2Button.isVisible()){
				this.helpText.setText("Nie możesz użyć tych kół ratunkowych razem");
			}else{
				this.callFriendButton.setDisable(true);
				this.helpText.setText(
					currentQuestion.getAnswersArray()[rand.ints(0, 3).findFirst().getAsInt()]);
		}
	}
}
}