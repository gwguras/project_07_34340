package pl.edu.anstar.milionerzy;

import pl.edu.anstar.milionerzy.ui.MilionerzyApplication;

/**
 * This class is required for proper project packaging as main class cannot
 * extend any other class in order for project to run
 */
public class Main {
	public static void main(String[] args) {
		MilionerzyApplication.main(args);
	}
}
