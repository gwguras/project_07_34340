package pl.edu.anstar.milionerzy.ui;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MilionerzyApplication extends Application {


	@Override
	public void start(Stage stage) throws IOException {
		Scene scene = new Scene(loadFXML(), 600, 400);
		stage.setScene(scene);
		stage.show();
	}

	static Parent loadFXML() throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(MilionerzyApplication.class.getResource("menu" + ".fxml"));
		return fxmlLoader.load();
	}



	public static void main(String[] args) {
		launch();
	}

}