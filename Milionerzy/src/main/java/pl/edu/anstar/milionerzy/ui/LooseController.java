package pl.edu.anstar.milionerzy.ui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class LooseController {
	@FXML
	Button moveMenuButton;
	
	public void onClickMoveMenuButton() {
		
		Platform.exit();
		System.exit(0);
	}
}
