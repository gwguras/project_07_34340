# Millionaires

## Table of contents
* [General info](#markdown-header-general-info)
* [Technologies](#markdown-header-technologies)
* [Instruction](#markdown-header-instruction)
* [Distribution of tasks](#markdown-header-distribution-of-tasks)
* [Created by](#markdown-header-created-by)

## General info
This project was created for univeristy purpose.

## Technologies
* Java- SDK version 17
* Maven
* openjfx
* CSV files


## Instruction
To run this project:
```
1. Run project from your IDE, with maven build and arugment 
in maven start, javafx:run
2. Play a game
3. After you loose or win, just click "Zakończ"
```

### Distribution of tasks ###

| Arkadiusz Nowak      | Anita Zych             | Szczepan Wójcik  |
|----------------------|------------------------|----------------- |
| Back-End and Testing | Front-End and Graphics | Database and API |
| Documentation        | Documentation          | Documentation    |


### Created by: ###
* Arkadiusz Nowak  [Bitbucket](https://bitbucket.org/cichyninja2137)
* Anita Zych       [Bitbucket](https://bitbucket.org/anison99_)
* Szczepan Wójcik  [Bitbucket](https://bitbucket.org/gwguras)